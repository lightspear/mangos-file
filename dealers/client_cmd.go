package dealers

import (
	"fmt"
	"m/setting"
	"time"

	"go.nanomsg.org/mangos/v3"
	"go.nanomsg.org/mangos/v3/protocol/req"

	// register ws transport
	_ "go.nanomsg.org/mangos/v3/transport/ws"
)

var commonsock *mangos.Socket = nil

func getsock_common(cfg *setting.GobalClientConf) *mangos.Socket {
	if commonsock != nil {
		return commonsock
	}

	sock, e := req.NewSocket()
	if e != nil {
		die("cannot make req socket: %v", e)
	}
	url := fmt.Sprintf("%s/cmd", cfg.WSServerUrl)
	if e = sock.Dial(url); e != nil {
		// die("cannot dial req url: %v", e)
		return getsock_common(cfg)
	}
	// Time for TCP connection set up
	sock.SetOption(mangos.OptionRecvDeadline, time.Second)
	sock.SetOption(mangos.OptionSendDeadline, time.Second)
	commonsock = &sock
	return commonsock
}
